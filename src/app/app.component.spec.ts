import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Addition } from './Calculator';

describe('AppComponent', () => {
  // var comp : AppComponent; --- for Day 2
  let component = new AppComponent; 
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AngularUnitTestApp'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('AngularUnitTestApp');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('AngularUnitTestApp app is running!');
  });

  // it('My TestCase',()=>{
  //   expect(true).toBe(true);
  // })

  // it('My show alert message',()=>{
  //  expect(component.showMessage("Hello")).toBe("Hello")
  // })

  // it('Show Addition result', ()=>{
  //   expect(Addition(10,20)).toBe(30)
  // })

  // it('Show Addition result should be greater than', ()=>{
  //   expect(Addition(10,20)).toBeGreaterThan(20);
  // })

  it('ToBe and ToEqual Test Case',()=>{
    // var a = "Hello";
    // var b = "Hello";
    // expect(a).toBe(b);
    // expect(a).toEqual(b); it will also pass to be will work for primitive data type like strings,numbers or boolean for else use toEqual
    
    // var a=[1]; 2nd example
    // var b = [1]
    // expect(a).toEqual(b);

    // var a = 'Hello'; 3rd example
    // expect(a).toBe('Hello');

    // var a = true; 4th example
    // expect(a).toBeTrue();

    // var a = false; 5th example
    // expect(a).toBeFalse();

    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.componentInstance;
    // expect(app.check).toBeTrue();

    // var a = true;  it will fail bcz undefined is not true
    // expect(undefined).toBeTruthy();

    // var a = true;  it will fail bcz toBeFalse will check false
    // expect(undefined).toBeFalse();

    // var a = true;
    // expect(undefined).toBeFalsy();

    var a=5;
    expect(a).toBeLessThanOrEqual(5);
  })

  // it('Jasmin Matcher - Match function',() =>{
  //   var input = 'The office tutorials';
  //   var strPhone = "001-789-56-67";
  //   expect(input).toMatch(/office/);
  //   expect(input).toMatch(/office/i);
  //   expect(input).not.toMatch(/off1/);
  //   expect(input).not.toMatch(/\d{3}-\d{3}-\d{2}-\d{2}/);
  // })

  // it('Jasmin Matcher - toBeDefined',function() {
  //   var MyObj = {
  //     foo: "foo"
  //   };
  //   var Myfunction = (function() {}) ();
  //   var strUndefined;
  //   var strdefined = 'Hello';
  //   expect(MyObj).toBeDefined();
  //   expect(MyObj.foo).toBeDefined();
  //   expect(Myfunction).not.toBeDefined();
  //   expect(strUndefined).not.toBeDefined();
  //   expect(strdefined).toBeDefined();
  // })

  // it('Jasmin Matcher - toBeNull',function() {
  //   var nullValue = null;
  //   var valueUndefined;
  //   var notNull = "notNull";
  //   expect(null).toBeNull();
  //   expect(nullValue).toBeNull();
  //   expect(valueUndefined).not.toBeNull();
  //   expect(notNull).not.toBeNull();
  // })

  // it('Jasmin Matcher - toContain',function() {
  //   var MyArray = ['jasmine','Dotnet','Tutorials'];
  //   expect([1,2,3]).toContain(2);
  //   expect([1,2,3]).toContain(2,3);
  //   expect(MyArray).toContain("jasmine");
  //   expect([1,2,3]).not.toContain(4);
  //   expect(MyArray).not.toContain("dot");
  // })  

  // it('Jasmin Matcher - tobeNan',function() {
  //   expect(0/0).toBeNaN();
  //   expect(0/5).not.toBeNaN();
  // }) 

  // beforeEach( function(){ ---------------------------------Day 2 start
  //   comp = new AppComponent;
  //   console.log("beforeEach")
  // })
  // afterEach( function(){
  //   comp = new AppComponent;
  //   console.log("afterEach")
  // })
  // it("Increase count", () =>{
  //   comp.increaseCount(2);
  //   expect(comp.count).toEqual(12);
  //   console.log("Increase")
  // })
  // it("Decrease Count", () =>{
  //   comp.decreaseCount(2);
  //   expect(comp.count).toEqual(8);
  //   console.log("Decrease")
  // })
  // it("test 1", () =>{
  //   console.log("test 1")
  // })
  // it("test 2", () =>{
  //   console.log("test 2")
  // })
  // it("test 3", () =>{
  //   console.log("test 3")
  // })
  

  // it('Test', ()=>{ ---------------------------Day 3 Start
  //     //Arrange
  // let comp = new AppComponent();

  // //Act
  // let msg = comp.showMessage('Hello')

  // //Assert
  // expect(msg).toBe('Hello')
  // }) ---------------------------Day 3 End

  it('Test', ()=>{
      //Arrange
  let comp = new AppComponent();

  //Act
  let msg = comp.showMessage('Hello')

  //Assert
  expect(msg).toBe('Hello')
  }) 
}); 
