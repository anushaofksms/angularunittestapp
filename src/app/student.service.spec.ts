
import { TestBed } from '@angular/core/testing';
import { StudentService } from './student.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


describe('StudentService', () => {

      beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientTestingModule], 
        providers: [StudentService]
      }));

       it('should be created', () => {
        const service: StudentService = TestBed.get(StudentService);
        expect(service).toBeTruthy();
       });

    });
