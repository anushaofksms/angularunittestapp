import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TeacherComponent } from './teacher.component';
import { HttpClientModule,HttpClient } from '@angular/common/http'; 
import { StudentService } from '../student.service';
import { HttpClientTestingModule } from '@angular/common/http/testing'; 
describe('TeacherComponent', () => {
  let component: TeacherComponent;
  let fixture: ComponentFixture<TeacherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeacherComponent ],
      imports:[HttpClientModule],
      providers: [StudentService,HttpClient,HttpClientTestingModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
