import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  sum:number = 0;
  constructor(public service:StudentService) { }
  result:any;
  studentResult:any;
  ngOnInit(): void {
  }

  calculate(num1:number,num2:number) {
    this.sum = num1 + num2;
    return this.sum;
  }

  saveData() {
    let info = {
      sumval: this.calculate(2,3),
      name: 'Dot Net Office'
    };
    this.saveToConsole(info);
    this.service.SaveDetails(info).subscribe((response:any) =>{
      this.result = response
    })
  }

  studentResults() {
    if(this.calculate(10,20) >= 40) {
      return 'Pass';
    } else {
      return 'Fail';
    }
  }

  saveToConsole(info:any) {
    console.log(info);
  }

  studentSchoolResults() {
    if(this.calculate(10,20) >= 40) {
      this.studentResult = 'Pass';
      return this.studentResult;
    } else {
      this.studentResult = 'Fail';
      return this.studentResult;
    }
  }
}
