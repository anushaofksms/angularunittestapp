import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StudentComponent } from './student.component';
import { StudentService } from '../student.service';
import {HttpClientModule,HttpClient} from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('StudentComponent', () => {
  let component: StudentComponent;
  let fixture: ComponentFixture<StudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentComponent ],
      providers: [StudentService,HttpClient],
      imports: [HttpClientTestingModule,HttpClientModule,RouterTestingModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("spy on",()=>{
    spyOn(component,'calculate');
    component.saveData();
    expect(component.calculate).toHaveBeenCalled();
  })

  it("spy on - 1",()=>{
    spyOn(component,'calculate').and.returnValues(10,20);
    let result = component.studentResults();
    expect(result).toEqual('Fail');
  })

  it("spy on - 2",()=>{
    let service = fixture.debugElement.injector.get(StudentService)
    spyOn(service,'SaveDetails').and.callFake(()=>{
      return of({
        'result1':200
      });
      spyOn(component,'saveToConsole').and.stub();
      component.saveData();
      expect(component.result).toEqual({
        'result1':200
      })
    });
  })

  it('Verify the element value',()=>{
    const h1 = fixture.nativeElement.querySelector('h1');
    component.studentSchoolResults();
    fixture.detectChanges();
    expect(h1.textContent).toBe(component.studentResult);
  })

  it('to be pass',()=>{
   spyOn(component,'calculate').and.returnValues(40)
   let result = component.studentSchoolResults();
   expect(result).toEqual('Pass')
  })

  it('to be fail',()=>{
    //1st method
    component.studentSchoolResults();
    spyOn(component,'calculate').and.returnValues(30)
    expect(component.studentResult).toEqual('Fail')

    //2nd --- so 1st and second both will work
    // spyOn(component,'calculate').and.returnValues(30)
    // component.studentSchoolResults();
    // expect(component.studentResult).toEqual('Fail')
   })
});
