import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularUnitTestApp';
  check = true;
  count:number = 10;
  showMessage(msg:string) {
    return msg;
  }

  checkValue(){
    this.check = false;
  }

  increaseCount(num:number) {
    this.count =  this.count + num;
  }

  decreaseCount(num:number) {
    this.count =  this.count - num;
  }
}
